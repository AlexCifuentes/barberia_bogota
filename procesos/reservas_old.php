<!DOCTYPE HTML>
<!--
	Dopetrope by HTML5 UP
	html5up.net | @ajlkn
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
	<head>
		<title><?=$titulo?></title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets-3/css/main.css" />
		<!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie8.css" /><![endif]-->
	</head>
	<body class="right-sidebar">
		<div id="page-wrapper">

			<!-- Header -->
				<div id="header-wrapper">
					<div id="header">

						<!-- Logo -->
							<h1><a href="index.html">Reservas</a></h1>

						<!-- Nav -->
							<nav id="nav">
								<ul>
									<li><a href="<?php echo site_url()?>">Inicio</a></li>
									<li>
										<a href="#">Dropdown</a>
										<ul>
											<li><a href="#">Lorem ipsum dolor</a></li>
											<li><a href="#">Magna phasellus</a></li>
											<li><a href="#">Etiam dolore nisl</a></li>
											<li>
												<a href="#">Phasellus consequat</a>
												<ul>
													<li><a href="#">Magna phasellus</a></li>
													<li><a href="#">Etiam dolore nisl</a></li>
													<li><a href="#">Veroeros feugiat</a></li>
													<li><a href="#">Nisl sed aliquam</a></li>
													<li><a href="#">Dolore adipiscing</a></li>
												</ul>
											</li>
											<li><a href="#">Veroeros feugiat</a></li>
										</ul>
									</li>
									<li><a href="left-sidebar.html">Left Sidebar</a></li>
									<li class="current"><a href="right-sidebar.html">Right Sidebar</a></li>
									<li><a href="no-sidebar.html">No Sidebar</a></li>
								</ul>
							</nav>

					</div>
				</div>

			<!-- Main -->
				<div id="main-wrapper">
					<div class="container">
						<div class="row">
							<div class="8u 12u(mobile)">

								<!-- Content -->
									<article class="box post">
										<a href="#" class="image featured"><img src="<?php echo base_url(); ?>images/bg.png" alt="" /></a>
										<header>
											<h2>Ingese Numero de Cedula</h2>
                                            <p>
                                            <input type="text" name="cedula" id="cedula">
                                            </p>
										</header>										
										<section>
											<header>
												<h3>Seleccione Fecha</h3>
											</header>
                                            <p>
                                                <input type="date" name="fecha" id="fecha">
                                            </p>											
										</section>
                                        <section>
											<header>
												<h3>Seleccione hora</h3>
											</header>
                                            <p>
                                                <input type="date" name="fecha" id="fecha">
                                            </p>											
										</section>
                                        <section>
											<header>
												<h3>Seleccione Barbero</h3>
											</header>
                                            <p>
                                                <input type="date" name="fecha" id="fecha">
                                            </p>											
										</section>
                                        <section>											
                                            <p>
                                                <a href="#" class="button alt">Reservar</a>
                                            </p>											
										</section>
                                        
									</article>

							</div>
							<div class="4u 12u(mobile)">

								<!-- Sidebar -->
									<section class="box">
										<a href="#" class="image featured"><img src="images/pic09.jpg" alt="" /></a>
										<header>
											<h3>Tutorial</h3>
										</header>
										<p>Si eres nuevo y quieres reservar oprime el boton y la aplicacion te guiara paso a paso de como realizar su reserva </p>
										<footer>
											<a href="#" class="button alt">Tutorial</a>
										</footer>
									</section>
									<section class="box">
										<header>
											<h3>MENU</h3>
										</header>										
										<ul class="divided">
											<li><a href="#">Inicio</a></li>
											<li><a href="#">Saber Tus Cortes</a></li>
										</ul>										
									</section>

							</div>
						</div>
					</div>
				</div>

			<!-- Footer -->
				<div id="footer-wrapper">
					<section id="footer" class="container">						
						<div class="row">													
							<div class="4u 12u(mobile)">
								<section>
									<header>
										<h2>Sigenos y Compartenos</h2>
									</header>
									<ul class="social">
										<li><a class="icon fa-facebook" href="#"><span class="label">Facebook</span></a></li>
										<li><a class="icon fa-twitter" href="#"><span class="label">Twitter</span></a></li>
										<li><a class="icon fa-dribbble" href="#"><span class="label">Dribbble</span></a></li>
										<li><a class="icon fa-linkedin" href="#"><span class="label">LinkedIn</span></a></li>
										<li><a class="icon fa-google-plus" href="#"><span class="label">Google+</span></a></li>
									</ul>									
								</section>
							</div>
						</div>
						<div class="row">
							<div class="12u">

								<!-- Copyright -->
									<div id="copyright">
										<ul class="links">
											<li>&copy; LicanSoft. Derechos reservados.</li><li>Diseño: <a href="http://html5up.net">HTML5 UP</a></li>
										</ul>
									</div>

							</div>
						</div>
					</section>
				</div>

		</div>

		<!-- Scripts -->
			<script src="<?php echo base_url(); ?>assets-3/js/jquery.min.js"></script>
			<script src="<?php echo base_url(); ?>assets-3/js/jquery.dropotron.min.js"></script>
			<script src="<?php echo base_url(); ?>assets-3/js/skel.min.js"></script>
			<script src="<?php echo base_url(); ?>assets-3/js/skel-viewport.min.js"></script>
			<script src="<?php echo base_url(); ?>assets-3/js/util.js"></script>
			<!--[if lte IE 8]><script src="assets/js/ie/respond.min.js"></script><![endif]-->
			<script src="<?php echo base_url(); ?>assets-3/js/main.js"></script>

	</body>
</html>