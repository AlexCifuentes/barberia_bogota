$(function(){

    var currentDate; // Holds the day clicked when adding a new event
    var currentEvent; // Holds the event object when editing an event
    var initialLangCode = 'es'; //define el idioma principal

    $('#color').colorpicker(); // Colopicker
    //se quita xq cambie a oto famework
    /*$('#time').timepicker({
        minuteStep: 300,
        showInputs: true,
        disableFocus: true,
        showMeridian: false,
        maxHours: 10,
        autoclose: 1,
		startView: 1,
		minView: 0,
		forceParse: 1,
        minuteStep: 30,
        //endHour:
    });  // Timepicker*/
    $('#time').datetimepicker({
                   format: 'HH:mm',
                   disabledHours: [0, 1, 2, 3, 4, 5, 6, 7, 8, 20, 21, 22, 23, 24],
                   stepping: 30,
                   showClose: true,
                });

    var base_url='http://localhost/fullcalendar/'; // Here i define the base_url
    

    // Fullcalendar
    $('#calendar').fullCalendar({
        timeFormat: 'H(:mm)',
        header: {
            left: 'prev, next, today',
            center: 'title',
            right: 'month, basicWeek, basicDay'
        },
        lang: initialLangCode,
        // Get all events stored in database

        eventLimit: true, // allow "more" link when too many events
        //var tal=base_url+'calendar/getEvents';
        events: base_url+'calendar/getEvents',
        
        //events: eventos[0],eventos[1],

        // Handle Day Click
        dayClick: function(date, event, view) {
            currentDate = date.format();
            // Open modal to add event
            modal({
                // Available buttons when adding
                buttons: {
                    add: {
                        id: 'add-event', // Buttons id
                        css: 'btn-success', // Buttons class
                        value: 'Submit', // Buttons value
                        //label: 'Add' // Buttons label
                        label: 'Reservar' // Buttons label
                        
                    }
                },
                //title: 'Add Event (' + date.format() + ')' // Modal title
                title: 'Reserva (' + date.format() + ')' // Modal title
            });
        },
        
   
          editable: true, // Make the event draggable true 
          eventDrop: function(event, delta, revertFunc) {  

            
            $.post(base_url+'calendar/dragUpdateEvent',{                            
                id:event.id,
                date: event.start.format()
            }, function(result){
                if(result)
                {
                alert('Updated');
                }
                else
                {
                    alert('Try Again later!')
                }

            });



          },
        // Event Mouseover
        eventMouseover: function(calEvent, jsEvent, view){

            var tooltip = '<div class="event-tooltip">' + calEvent.cedula + '</div>';
            $("body").append(tooltip);

            $(this).mouseover(function(e) {
                $(this).css('z-index', 10000);
                $('.event-tooltip').fadeIn('500');
                $('.event-tooltip').fadeTo('10', 1.9);
            }).mousemove(function(e) {
                $('.event-tooltip').css('top', e.pageY + 10);
                $('.event-tooltip').css('left', e.pageX + 20);
            });
        },
        eventMouseout: function(calEvent, jsEvent) {
            $(this).css('z-index', 8);
            $('.event-tooltip').remove();
        },
        // Handle Existing Event Click
        eventClick: function(calEvent, jsEvent, view) {
            // Set currentEvent variable according to the event clicked in the calendar
            currentEvent = calEvent;

            // Open modal to edit or delete event
            modal({
                // Available buttons when editing
                buttons: {
                    delete: {
                        id: 'delete-event',
                        css: 'btn-danger',
                        label: 'Borrar'
                    },
                    update: {
                        id: 'update-event',
                        css: 'btn-success',
                        label: 'Actualizar'
                    }
                },
                //title: 'Edit Event "' + calEvent.title + '"',
                title: 'Editar reserva "' + calEvent.cedula + '"',
                event: calEvent
            });
        }

    });
    

        // build the language selector's options
        $.each($.fullCalendar.langs, function(langCode) {
            $('#lang-selector').append(
                $('<option/>')
                    .attr('value', langCode)
                    .prop('selected', langCode === initialLangCode)
                    .text(langCode)
            );
        });

        // when the selected option changes, dynamically change the calendar option
        $('#lang-selector').on('change', function() {
            if (this.value) {
                $('#calendar').fullCalendar('option', 'lang', this.value);
            }
        });
    
    

    // Prepares the modal window according to data passed
    function modal(data) {
        // Set modal title
        $('.modal-title').html(data.title);
        // Clear buttons except Cancel
        $('.modal-footer button:not(".btn-default")').remove();
        // Set input values
        $('#cedula').val(data.event ? data.event.cedula : '');
        if( ! data.event) {
            // When adding set timepicker to current time
            var now = new Date();
            //var time = now.getHours() + ':' + (now.getMinutes() < 10 ? '0' + now.getMinutes() : now.getMinutes());
            var time = now.getHours() + ':' + (now.getMinutes() > 1 ? '30': '00');
        } else {
            // When editing set timepicker to event's time
            var time = data.event.date.split(' ')[1].slice(0, -3);
            time = time.charAt(0) === '0' ? time.slice(1) : time;
        }
        $('#time').val(time);
        //$('#idbarbero').val(data.event ? data.event.idbarbero : ''); se quita por ser un select
        $('#color').val(data.event ? data.event.color : '#3a87ad');
        // Create Butttons
        $.each(data.buttons, function(index, button){
            $('.modal-footer').prepend('<button type="button" id="' + button.id  + '" class="btn ' + button.css + '" value="' + button.value + '">' + button.label + '</button>')
        })
        //Show Modal
        $('.modal').modal('show');
    }

    // Handle Click on Add Button
    $('.modal').on('click', '#add-event',  function(e){
        if(validator(['cedula', 'idbarbero'])) {
            //$.post(base_url+'calendar/addEvent', {
            $.post(base_url+'calendar/validaDatos', {            
                cedula: $('#cedula').val(),
                idbarbero: $('#idbarbero').val(),
                color: $('#color').val(),
                date: currentDate + ' ' + getTime()
            }, function(result){
                /*reciv rwespuestas del servidor y valido*/
                if(result == "fechaPaso") {
                    alert("La fecha que has seleccionado ya paso por favor selecciona una correcta ");
                }if(result == "ocupado") {
                    alert("El turno o barbero escogido se encuentra ya ocupado elija otra fecha u hora");
                }
                if(result == "descansando") {
                    alert("El barbero se encuentra descansando ese dia por favor seleccine otro barbero");
                }if(result == "horaPaso") {
                    alert("No Puede reservar en horas anteriores a la actual");
                }
                else{
                $('.modal').modal('hide');
                $('#calendar').fullCalendar("refetchEvents");
            }
            });
        }
    });


    // Handle click on Update Button
    $('.modal').on('click', '#update-event',  function(e){
        if(validator(['cedula', 'idbarbero'])) {
            $.post(base_url+'calendar/updateEvent', {
                id: currentEvent._id,
                cedula: $('#cedula').val(),
                idbarbero: $('#idbarbero').val(),
                color: $('#color').val(),
                //date: currentEvent.date.split(' ')[0]  + ' ' +  getTime()//se quita para no permitir actualizar fecha
            }, function(result){
                $('.modal').modal('hide');
                $('#calendar').fullCalendar("refetchEvents");
            });
        }
    });



    // Handle Click on Delete Button
    $('.modal').on('click', '#delete-event',  function(e){
        $.get(base_url+'calendar/deleteEvent?id=' + currentEvent._id, function(result){
            $('.modal').modal('hide');
            $('#calendar').fullCalendar("refetchEvents");
        });
    });


    // Get Formated Time From Timepicker
    function getTime() {
        var time = $('#time').val();
        return (time.indexOf(':') == 1 ? '0' + time : time) + ':00';
    }

    // Dead Basic Validation For Inputs
    function validator(elements) {
       var errors = 0;
       var horaCerrado1= 859;
       var horaCerrado2= 1930;
       var hora= $('#time').val();
       var hora2= hora.replace(':','');
        
        $.each(elements, function(index, element){
            
            //if($.trim($('#' + element).val()) == '') errors++;
            //valido que los imput no esten vacios
            if($('#cedula').val() == ''){
                $('.error').html('Por favor ingrese la cedula');
                errors++;
                }
            //valido que tenga mas de 8 caracteres
            if($('#cedula').val().length < 8){
                $('.error').html('Por favor ingrese un valor correcto para la cedula');
                errors++;
                }
            //valido que campo hora
            if($('#time').val() == ''){
                $('.error').html('Por favor ingese un valor en el campo hora');
                 errors++;
                }
            //valido que campo hora
            if(hora2 < horaCerrado1 || hora2 > horaCerrado2 ){
                $('.error').html('No atendemos en esos horarios');
                 errors++;
                }
        });
        if(errors) {
            //$('.error').html('Please insert title and description');
            return false;
        }
        return true;                
    }
    
    
});