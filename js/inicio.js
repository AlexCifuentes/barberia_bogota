//var base_url='http://localhost/barberia_bogota/'; // Defino el de la aplicacion base_url
var getUrl = window.location;
var baseUrl = getUrl .protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];
//console.log(baseUrl);

jQuery(function($){

listarCortes();

});

/**
| ajax carga cortes de pelo|
|*/
function listarCortes() {
    
    var liCortes = $('#liCortes');
     // Obtener la instancia del objeto XMLHttpRequest
    jQuery.ajax({
    type: "POST",
    url: baseUrl + "/index.php/Welcome/get_cortes",
    dataType: 'json',
    //data: {name: user_name, pwd: password},
    success: function(data) {
        
        $.each(data,function(k,v){
            console.log(v);
            var nombre = v.nombre.replace('.jpg','');
            var contenido='<li class="item-thumbs span3 cortes">'+ 
                            '<!-- Fancybox - Gallery Enabled - Title - Full Image -->'+
                                '<a class="hover-wrap fancybox" data-fancybox-group="gallery" title="Corte '+k+'" href="'+v.ruta+'">'+
                                    '<span class="overlay-img"></span>'+
                                    '<span class="overlay-img-thumb font-icon-plus"></span>'+
                                '</a>'+
                            '<!-- Thumb Image and Description -->'+
                            '<img src="'+v.ruta+'" alt="Corte de pelo '+nombre+'" style="width:300px;height:200px;"/>'+
                        '</li>';

            liCortes.append(contenido);
        
        
        });

    }
    });
	
}

/**
| ajax carga barbas|
|*/
function listarBarbas() {
    var liCortes = $('#liBarbas');
     // Obtener la instancia del objeto XMLHttpRequest
    jQuery.ajax({
    type: "POST",
    url: baseUrl + "/index.php/Welcome/get_barbas",
    dataType: 'json',
    //data: {name: user_name, pwd: password},
    success: function(data) {
        
        $.each(data,function(k,v){
            console.log(v);
            var nombre = v.nombre.replace('.jpg','');
            var contenido='<li class="item-thumbs span3 barbas">'+
                                                '<!-- Fancybox - Gallery Enabled - Title - Full Image -->'
                                                '<a class="hover-wrap fancybox" data-fancybox-group="gallery" title="Corte numero '+k+'" href="'+v.ruta+'">'
                                                    '<span class="overlay-img"></span>'+
                                                    '<span class="overlay-img-thumb font-icon-plus"></span>'+
                                                '</a>'+
                                                '<!-- Thumb Image and Description -->'+
                                                '<img src="'+v.ruta+'">'+
                                            '</li>';

            liCortes.append(contenido);
        
        
        });

    }
    });
}

/**
| ajax carga cortes de cejas|
|*/
function listarCejas() {
    var liCortes = $('#liCejas');
     // Obtener la instancia del objeto XMLHttpRequest
    jQuery.ajax({
    type: "POST",
    url: baseUrl + "/index.php/Welcome/get_cejas",
    dataType: 'json',
    //data: {name: user_name, pwd: password},
    success: function(data) {
        
        $.each(data,function(k,v){
            console.log(v);
            var nombre = v.nombre.replace('.jpg','');
            var contenido='<li class="item-thumbs span3 cejas">'+
                                            ' <!-- Fancybox - Gallery Enabled - Title - Full Image -->'+
                                                '<a class="hover-wrap fancybox" data-fancybox-group="gallery" title="Corte numero '+k+'" href="">'+
                                                    '<span class="overlay-img"></span>'+
                                                    '<span class="overlay-img-thumb font-icon-plus"></span>'+
                                                '</a>'+
                                            ' <!-- Thumb Image and Description -->'+
                                            ' <img src="">'+
                                            '</li>';

            liCortes.append(contenido);
        
        
        });

    }
    });
}