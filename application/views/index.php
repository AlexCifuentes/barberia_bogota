<!--VISTA PRINCIPAL-->
<!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if (IE 7)&!(IEMobile)]><html class="no-js lt-ie9 lt-ie8" lang="en"><![endif]-->
<!--[if (IE 8)&!(IEMobile)]><html class="no-js lt-ie9" lang="en"><![endif]-->
<!--[if (IE 9)]><html class="no-js ie9" lang="en"><![endif]-->
<!--[if gt IE 8]><!--> <html lang="en-US"> <!--<![endif]-->
<head>

<!-- Meta Tags -->
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

<title>Barberia Bogota | Cortes-Barbas</title>   

<meta name="description" content="pagina de cortes para hombres" /> 

<!-- Mobile Specifics -->
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="HandheldFriendly" content="true"/>
<meta name="MobileOptimized" content="320"/>   

<!-- Mobile Internet Explorer ClearType Technology -->
<!--[if IEMobile]>  <meta http-equiv="cleartype" content="on">  <![endif]-->

<!-- Bootstrap -->
<link href="_include/css/bootstrap.min.css" rel="stylesheet">

<!-- Main Style -->
<link href="_include/css/main.css" rel="stylesheet">

<!-- Supersized -->
<link href="_include/css/supersized.css" rel="stylesheet">
<link href="_include/css/supersized.shutter.css" rel="stylesheet">

<!-- FancyBox -->
<link href="_include/css/fancybox/jquery.fancybox.css" rel="stylesheet">

<!-- Font Icons -->
<link href="_include/css/fonts.css" rel="stylesheet">

<!-- Shortcodes -->
<link href="_include/css/shortcodes.css" rel="stylesheet">

<!-- Responsive -->
<link href="_include/css/bootstrap-responsive.min.css" rel="stylesheet">
<link href="_include/css/responsive.css" rel="stylesheet">

<!-- Supersized -->
<link href="_include/css/supersized.css" rel="stylesheet">
<link href="_include/css/supersized.shutter.css" rel="stylesheet">

<!-- Google Font -->
<link href='http://fonts.googleapis.com/css?family=Titillium+Web:400,200,200italic,300,300italic,400italic,600,600italic,700,700italic,900' rel='stylesheet' type='text/css'>

<!-- Fav Icon -->
<link rel="shortcut icon" href="#">

<link rel="apple-touch-icon" href="#">
<link rel="apple-touch-icon" sizes="114x114" href="#">
<link rel="apple-touch-icon" sizes="72x72" href="#">
<link rel="apple-touch-icon" sizes="144x144" href="#">

<!-- Modernizr -->
<script src="_include/js/modernizr.js"></script>

<!-- Analytics -->
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'Insert Your Code']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<!-- End Analytics -->

</head>


<body>

<!-- This section is for Splash Screen -->
<div class="ole">
<section id="jSplash">
	<div id="circle"></div>
</section>
</div>
<!-- End of Splash Screen -->

<!-- Homepage Slider -->
<div id="home-slider">	
    <div class="overlay"></div>

    <div class="slider-text">
    	<div id="slidecaption"></div>
    </div>   
	
	<div class="control-nav">
        <a id="prevslide" class="load-item"><i class="font-icon-arrow-simple-left"></i></a>
        <a id="nextslide" class="load-item"><i class="font-icon-arrow-simple-right"></i></a>
        <ul id="slide-list"></ul>
        
        <a id="nextsection" href="#work"><i class="font-icon-arrow-simple-down"></i></a>
    </div>
</div>
<!-- End Homepage Slider -->

<!-- Header -->
<header>
    <div class="sticky-nav">
    	<a id="mobile-nav" class="menu-nav" href="#menu-nav"></a>
        
        <div id="logo">
        	<a id="goUp" href="#home-slider" title="Brushed | Responsive One Page Template">Baberia Bogota</a>
        </div>
        
        <nav id="menu">
        	<ul id="menu-nav">
            	<li class="current"><a href="#home-slider">Inicio</a></li>
                <li><a href="#work">Nuestro Trabajo</a></li>
                <li><a href="#about">Servicios</a></li>
                <li><a href="#app">App</a></li>
                <li><a href="#contact">Contactenos</a></li>
                <li><a href="<?php echo base_url();?>calendar" class="external" >Reservar</a></li>
				<li><a href="<?php echo site_url('Usuario/index') ?>" class="external" >Tus Cortes</a></li>
				<li><a href="<?php echo site_url('administration/Secure_bcrypt') ?>" class="external" >Admnistrativo</a></li>
                <li><a href="<?php echo site_url('Welcome/carga_imagenes') ?>">test</a></li>
                <li><a href="<?php echo base_url('Rest_server/index') ?>">test2</a></li>
            </ul>
        </nav>
        
    </div>
</header>
<!-- End Header -->

<!-- Our Work Section -->
<div id="work" class="page">
	<div class="container">
    	<!-- Title Page -->
        <div class="row">
            <div class="span12">
                <div class="title-page">
                    <h2 class="title">Un lugar para Hombres con Estilo</h2>
                    <h3 class="title-description">Cortes, Barbas, Cejas <a href="#">.</a></h3>
                </div>
            </div>
        </div>
        <!-- End Title Page -->
        
        <!-- Portfolio Projects -->
        <div class="row">
        	<div class="span3">
            	<!-- Filter -->
                <nav id="options" class="work-nav">
                    <ul id="filters" class="option-set" data-option-key="filter">
                    	<li class="type-work">Trabajos</li>
                        <!--<li><a href="#filter" data-option-value="*" class="selected">Trabajos</a></li>-->
                        <li><a href="#filter" data-option-value=".nosotros" class="selected">Nosotros</a></li>
                        <li><a href="#filter" data-option-value=".cortes" >Cortes</a></li>
                        <li><a href="#filter" data-option-value=".barbas">Barbas</a></li>
                        <li><a href="#filter" data-option-value=".cejas">Cejas</a></li>
                        <li><a href="#filter" data-option-value=".video">Videos</a></li>
                    </ul>
                </nav>
                <!-- End Filter -->
            </div>
            
            <div class="span9">
            	<div class="row">
                	<section id="projects">
                    	<ul id="thumbs">
                             <!-- Item Project and Filter NOSOTROS mision-->
                        	<li class="item-thumbs span3 nosotros">
                            	<!-- Fancybox - Gallery Enabled - Title - Full Image -->
                            	<a class="hover-wrap fancybox" data-fancybox-group="gallery" title="Mision"
                                   href="images/aplicacion/mision/mision.jpg">
                                    <span class="overlay-img"></span>
                                    <span class="overlay-img-thumb font-icon-plus"></span>
                                </a>
                                <!-- Thumb Image and Description -->
                                <img src="images/aplicacion/mision/mision.jpg" alt="Barberia Bogota." style="width:300px;height:300px;" />
                            </li>
                        	<!-- End Item NOSOTROS -->
                            <!-- Item Project and Filter NOSOTROS vision-->
                        	<li class="item-thumbs span3 nosotros">
                            	<!-- Fancybox - Gallery Enabled - Title - Full Image -->
                            	<a class="hover-wrap fancybox" data-fancybox-group="gallery" title="vision"
                                   href="images/aplicacion/nosotros/vision/vision.jpg">
                                	<span class="overlay-img"></span>
                                    <span class="overlay-img-thumb font-icon-plus"></span>
                                </a>
                                <!-- Thumb Image and Description -->
                                <img src="images/aplicacion/nosotros/vision/vision.jpg" alt="Ser la barberia." style="width:300px;height:300px;" />
                            </li>
                        	<!-- End Item NOSOTROS vision-->
                            
                            <!-- Item Project and Filter CORTES -->
                            <div id="liCortes"></div>
                           
                            <!-- End Item CORTES -->
                            
                            <!-- Item Project and Filter BARBAS -->
                            <div id="liBarbas"></div>
                            
                            <!-- End Item BARBAS -->
                        
				<!-- Item Project and Filter CEJAS -->
                                <div id="liCejas"></div>	
                        	<!-- End Item Project CEJAS-->
                            
				<!-- Item Project and Filter VIDEOS -->
                        	<li class="item-thumbs span3 video">
                            	<!-- Fancybox Media - Gallery Enabled - Title - Link to Video -->
                            	<a class="hover-wrap fancybox-media" data-fancybox-group="video" title="Video Content From Vimeo" href="http://vimeo.com/51460511">
                                	<span class="overlay-img"></span>
                                    <span class="overlay-img-thumb font-icon-plus"></span>
                                </a>
                                <!-- Thumb Image and Description -->
                                <img src="_include/img/work/thumbs/image-08.jpg" alt="Video">
                            </li>
                        	<!-- End Item Project -->
                        	
                        </ul>
                    </section>
                    
            	</div>
            </div>
        </div>
        <!-- End Portfolio Projects -->
    </div>
</div>
<!-- End Our Work Section -->

<!-- About Section -->
<div id="about" class="page-alternate">
<div class="container">
    <!-- Title Page -->
    <div class="row">
        <div class="span12">
            <div class="title-page">
                <h2 class="title">Servicios</h2>
                <!--<h3 class="title-description">Learn About our Team &amp; Culture.</h3>-->
            </div>
        </div>
    </div>
    <!-- End Title Page -->
    
    <!-- People -->
    <div class="row">
    	
        <!-- Start Profile -->
    	<div class="span4 profile">
        	<div class="image-wrap">
                <div class="hover-wrap">
                    <span class="overlay-img"></span>
                    <span class="overlay-text-thumb">Barberia</span>
                </div>
                <img src="_include/img/profile/profile-01.jpg" alt="John Doe">
            </div>
            <!--<h3 class="profile-name">Baberia Bogota</h3>-->
            <p class="profile-description">Desde este modulo puedes interactuar con nosotros, realizar <a href="#">Reservas</a>, 
            Mira la cantidad de <a href="#">cortes</a> que tienes, pide la <a href="#">musica</a> que deseas escuchar mientras estas en la baberia.
            Tambien contamos con wifi, video juegos, juegos de mesa, y mas.</p>
            	
            <div class="social">
            	<ul class="social-icons">
                	<li><a href="#"><i class="font-icon-social-twitter"></i></a></li>
                    <li><a href="#"><i class="font-icon-social-dribbble"></i></a></li>
                    <li><a href="#"><i class="font-icon-social-facebook"></i></a></li>
                </ul>
            </div>
        </div>
        <!-- End Profile -->
        
        <!-- Start Profile -->
    	<div class="span4 profile">
        	<div class="image-wrap">
                <div class="hover-wrap">
                    <span class="overlay-img"></span>
                    <span class="overlay-text-thumb">Ropa</span>
                </div>
                <img src="_include/img/profile/profile-02.jpg" alt="Jane Helf">
            </div>
            <!--<h3 class="profile-name">Tienad Helf</h3>-->
            <p class="profile-description">Visita nuestra <a href="#">tienda de ropa</a>. 
            Donde contamos con ropa de moda con estilos modernos para hombres y mujeres.</p>
            	
            <div class="social">
            	<ul class="social-icons">
                	<li><a href="#"><i class="font-icon-social-twitter"></i></a></li>
                    <li><a href="#"><i class="font-icon-social-email"></i></a></li>
                </ul>
            </div>
        </div>
        <!-- End Profile -->
        
        <!-- Start Profile -->
    	<div class="span4 profile">
        	<div class="image-wrap">
                <div class="hover-wrap">
                    <span class="overlay-img"></span>
                    <span class="overlay-text-thumb">Tatuajes</span>
                </div>
                <img src="_include/img/profile/profile-03.jpg" alt="Joshua Insanus">
            </div>
            <h3 class="profile-name">Alejandro Salaz</h3>
            <p class="profile-description">Piensas en tatuarte pasa por la barberia y encontraras el lugar especial donde <a href="#">Alejandro Salaz</a> un experto tatuador que trabajara sobre tu cuerpo .</p>
            	
            <div class="social">
            	<ul class="social-icons">
                	<li><a href="#"><i class="font-icon-social-twitter"></i></a></li>
                    <!--<li><a href="#"><i class="font-icon-social-linkedin"></i></a></li>
                    <li><a href="#"><i class="font-icon-social-google-plus"></i></a></li>
                    <li><a href="#"><i class="font-icon-social-vimeo"></i></a></li>-->
                </ul>
            </div>
        </div>
        <!-- End Profile -->
        
        <!-- Start Profile -->
    	<div class="span4 profile">
        	<div class="image-wrap">
                <div class="hover-wrap">
                    <span class="overlay-img"></span>
                    <span class="overlay-text-thumb">Manicure</span>
                </div>
                <img src="_include/img/profile/profile-04.jpg" alt="Joshua Insanus">
            </div>
            <h3 class="profile-name">Chamaca</h3>
            <p class="profile-description">No te inpacientes mientras esperas tu turno pide a <a href="#">Chamaca</a> un manicure, o tratamientos faciales.</p>
            	
            <div class="social">
            	<ul class="social-icons">
                	<li><a href="#"><i class="font-icon-social-twitter"></i></a></li>
                    <!--<li><a href="#"><i class="font-icon-social-linkedin"></i></a></li>-->
                    <li><a href="#"><i class="font-icon-social-google-plus"></i></a></li>
                    <li><a href="#"><i class="font-icon-social-vimeo"></i></a></li>
                </ul>
            </div>
        </div>
        <!-- End Profile -->
        
    </div>
    <!-- End People -->
</div>
</div>
<!-- End About Section -->

<!-- Apps Section -->
<div id="app" class="page">
<div class="container">
    <!-- Title Page -->
    <div class="row">
        <div class="span10">
            <div class="title-page">
                <h2 class="title">Nuestra Aplicacion</h2>
                <h3 class="title-description">Descarga nuestra aplicacion del las tienda de Goole play y APP store.</h3>
            </div>
        </div>
    </div>
    <!-- End Title Page -->
    
    <!-- container -->
    <div class="row">
    	<div class="span6">
            
        <div class="image-wrap">
                <div class="hover-wrap">
                    <span class="overlay-img"></span>
                    <span class="overlay-text-thumb">Barberia APP</span>
                </div>
                <img src="_include/img/profile/profile-03.jpg" alt="Joshua Insanus">
            </div>	
         
        </div>
        
        <div class="span6">
        	<div class="contact-details">
        		<h3>Codigo QR</h3>
                <ul>
                    <table class="table">
                        <thead>
                          <tr>
                            <th><a href="#">Apps Store</a></th>
                            <th><a href="#">Google Play</a></th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>
                                <div class="span2">
                                    <div class="image-wrap">
                                        <img src="_include/img/profile/qrIOS.jpg">
                                    </div>
                                </div>
                              </td>
                            <td>
                            <div class="span2">
                                <div class="image-wrap">
                                    <img src="_include/img/profile/qrAndroid.jpg">
                                </div>
                            </div>  
                            </td>
                          </tr>
                        </tbody>
                      </table>
                    
                    
                </ul>
            </div>
        </div>
    </div>
    <!-- End Container -->
</div>
</div>
<!-- End App Section -->

<!-- Contact Section -->
<div id="contact" class="page">
<div class="container">
    <!-- Title Page -->
    <div class="row">
        <div class="span12">
            <div class="title-page">
                <h2 class="title">Envianos tus Comentarios</h2>
                <h3 class="title-description">Nuestro Servicio son ustedes envianos tus quejas, reclamos, opiniones o cualquier cosa que nos ayude a mejorar nuestro servicio.</h3>
            </div>
        </div>
    </div>
    <!-- End Title Page -->
    
    <!-- Contact Form -->
    <div class="row">
    	<div class="span9">
        
        	<form id="contact-form" class="contact-form" action="#">
            	<p class="contact-name">
            		<input id="contact_name" type="text" placeholder="Nombre" value="" name="name" />
                </p>
                <p class="contact-email">
                	<input id="contact_email" type="text" placeholder="Email" value="" name="email" />
                </p>
                <p class="contact-message">
                	<textarea id="contact_message" placeholder="Mensage" name="message" rows="15" cols="40"></textarea>
                </p>
                <p class="contact-submit">
                	<a id="contact-submit" class="submit" href="#">Enviar</a>
                </p>
                
                <div id="response">
                
                </div>
            </form>
         
        </div>
        
        <div class="span3">
        	<div class="contact-details">
        		<h3>Detalles de Contacto </h3>
                <ul>
                    <li><a href="#">barberiabogota@gmail.com</a></li>
                    <li>(+57) 200-2525</li>
                    <li>
                        Barrio Chicala (BOSA)
                        <br>
                        Celular: 3157706565
                        <br>
                        
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <!-- End Contact Form -->
</div>
</div>
<!-- End Contact Section -->
    


<!-- Twitter Feed -->
<div id="twitter-feed" class="page-alternate">
	<div class="container">
    	<div class="row">
            <div class="span12">
                <div class="follow">
                    <a href="https://twitter.com/Bluxart" title="Follow Me on Twitter" target="_blank"><i class="font-icon-social-twitter"></i></a>
                    
                   
                </div>
                    
                <div id="ticker" class="query"> 
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Twitter Feed -->

<!-- Socialize -->
<div id="social-area" class="page">
	<div class="container">
    	<div class="row">
            <div class="span12">
                <nav id="social">
                    <ul>
                        <li><a href="https://twitter.com/Bluxart" title="Follow Me on Twitter" target="_blank"><span class="font-icon-social-twitter"></span></a></li>
                        <li><a href="http://dribbble.com/Bluxart" title="Follow Me on Dribbble" target="_blank"><span class="font-icon-social-dribbble"></span></a></li>
                        <li><a href="http://forrst.com/people/Bluxart" title="Follow Me on Forrst" target="_blank"><span class="font-icon-social-forrst"></span></a></li>
                        <li><a href="http://www.behance.net/alessioatzeni" title="Follow Me on Behance" target="_blank"><span class="font-icon-social-behance"></span></a></li>
                        <li><a href="https://www.facebook.com/Bluxart" title="Follow Me on Facebook" target="_blank"><span class="font-icon-social-facebook"></span></a></li>
                        <li><a href="https://plus.google.com/105500420878314068694" title="Follow Me on Google Plus" target="_blank"><span class="font-icon-social-google-plus"></span></a></li>
                        <li><a href="http://www.linkedin.com/in/alessioatzeni" title="Follow Me on LinkedIn" target="_blank"><span class="font-icon-social-linkedin"></span></a></li>
                        <li><a href="http://themeforest.net/user/Bluxart" title="Follow Me on ThemeForest" target="_blank"><span class="font-icon-social-envato"></span></a></li>
                        <li><a href="http://zerply.com/Bluxart/public" title="Follow Me on Zerply" target="_blank"><span class="font-icon-social-zerply"></span></a></li>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
</div>
<!-- End Socialize -->

<!-- Footer -->
<footer>
	<p class="credits">&copy;2016 Desarrollo: <a href="#" title="LIKANSOFT | Desarrollo a medida">LikanSoft</a> Diseño: Brushed. Crafted by <a href="http://designscrazed.org/" title="Brushed | Responsive One Page Template">Allie</a></p>
</footer>
<!-- End Footer -->

<!-- Back To Top -->
<a id="back-to-top" href="#">
	<i class="font-icon-arrow-simple-up"></i>
</a>
<!-- End Back to Top -->


<!-- Js -->
<!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>  jQuery Core -->
<script src="js/jquery/jquery-2.2.1.min.js"></script> <!-- jQuery Core -->
<script src="_include/js/bootstrap.min.js"></script> <!-- Bootstrap -->
<script src="_include/js/supersized.3.2.7.min.js"></script> <!-- Slider -->
<script src="_include/js/waypoints.js"></script> <!-- WayPoints -->
<script src="_include/js/waypoints-sticky.js"></script> <!-- Waypoints for Header -->
<script src="_include/js/jquery.isotope.js"></script> <!-- Isotope Filter -->
<script src="_include/js/jquery.fancybox.pack.js"></script> <!-- Fancybox -->
<script src="_include/js/jquery.fancybox-media.js"></script> <!-- Fancybox for Media -->
<script src="_include/js/jquery.tweet.js"></script> <!-- Tweet -->
<script src="_include/js/plugins.js"></script> <!-- Contains: jPreloader, jQuery Easing, jQuery ScrollTo, jQuery One Page Navi -->
<script src="_include/js/main.js"></script> <!-- Default JS -->
<script src="js/inicio.js"></script> <!-- JS operaciones ajax -->
<!-- End Js -->

</body>
</html>