<!DOCTYPE HTML>
<!--
	Identity by HTML5 UP
	html5up.net | @ajlkn
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
esta pagina tiene un mal diseño se devido a conflicto con stilos psteiormente corregir
-->
<html>
	<head>
		<title><?=$titulo?></title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<!--[if lte IE 8]><script src="assets/js/html5shiv.js"></script><![endif]-->
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets-2/css/main.css" />
		<!--[if lte IE 9]><link rel="stylesheet" href="assets/css/ie9.css" /><![endif]-->
		<!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie8.css" /><![endif]-->
		<noscript><link rel="stylesheet" href="<?php echo base_url(); ?>assets-2/css/noscript.css" /></noscript>
        <link href="<?php echo base_url(); ?>dropzone/dist/min/dropzone.min.css" type="text/css" rel="stylesheet" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>css/photoswipe/photoswipe.css"> 
	</head>
	<body class="is-loading">
        
        
		<!-- Wrapper -->
			<div id="wrapper">
				<!-- Main -->
					<section id="main">                        
						<header>
							<span class="avatar">
                            <ul class="icons">
								<li><a onclick="goBack()" class="fa-arrow-left">volver</a></li>
								<li><a href="<?php echo site_url('')?>" class="fa-home">Inicio</a></li>
							</ul>
                            </span>
							<h1>Subir Imagenes</h1>
							<p>Arrastre y suelte achivos, o solo haga click dentro del cuadro</p>						                        
                       
                        </header>

                        <div class="picture" itemscope itemtype="http://schema.org/ImageGallery">
                          <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                              <a href="#" itemprop="contentUrl" data-size="1000x667" data-index="0">
                                 <img src="<?php echo base_url(); ?>images/aplicacion/145.jpg" height="400" width="600" itemprop="thumbnail" alt="Beach">
                               </a>
                          </figure>
                          <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                             <a href="img/city-2.jpg" itemprop="contentUrl" data-size="1000x667" data-index="1">
                                <img src="img/city-2-thumb.jpg" height="400" width="600" itemprop="thumbnail" alt="">
                             </a>
                          </figure>
                        </div>

                          <div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">
                              <div class="pswp__bg"></div>
                                  <div class="pswp__scroll-wrap"> 
        <div class="pswp__container">
            <div class="pswp__item"></div>
            <div class="pswp__item"></div>
            <div class="pswp__item"></div>
        </div>
 
        <div class="pswp__ui pswp__ui--hidden">
            <div class="pswp__top-bar">
                <div class="pswp__counter"></div>
                <button class="pswp__button pswp__button--close" title="Close (Esc)"></button>
                <button class="pswp__button pswp__button--share" title="Share"></button>
                <button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>
                <button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>
                <div class="pswp__preloader">
                    <div class="pswp__preloader__icn">
                      <div class="pswp__preloader__cut">
                        <div class="pswp__preloader__donut"></div>
                      </div>
                    </div>
                </div>
            </div>
            <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
                <div class="pswp__share-tooltip"></div> 
            </div>
            <button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)">
            </button>
            <button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)">
            </button>
            <div class="pswp__caption">
                <div class="pswp__caption__center"></div>
            </div>
        </div>
    </div>
</div>
                        

                        
					</section>

				<!-- Footer -->
					<footer id="footer">
						<ul class="copyright">
							<li>&copy;</li><li>Design: <a href="http://html5up.net">HTML5 UP</a></li>
						</ul>
					</footer>

			</div>
        

		<!-- Scripts -->
        <script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
         <!-- Scripts contol de las imagenes -->
        <script src="<?php echo base_url(); ?>js/photoswipe/photoswipe.min.js"></script> 
        <script src="<?php echo base_url(); ?>js/photoswipe/photoswipe-ui-default.min.js"></script>
        <script src="<?php echo base_url(); ?>js/imagenes/imagenes.js"></script>
        <!-- Scripts contol de dropzone -->
        <script src="<?php echo base_url(); ?>dropzone/dist/min/dropzone.min.js"></script>
			<!--[if lte IE 8]><script src="assets/js/respond.min.js"></script><![endif]-->
			<script>
				if ('addEventListener' in window) {
					window.addEventListener('load', function() { document.body.className = document.body.className.replace(/\bis-loading\b/, ''); });
					document.body.className += (navigator.userAgent.match(/(MSIE|rv:11\.0)/) ? ' is-ie' : '');
				}
			</script>
            <script>
                function goBack() {
                    window.history.back();
                }
            </script>
	</body>
</html>