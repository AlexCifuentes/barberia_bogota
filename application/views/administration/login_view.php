<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title><?=$titulo?></title>
<link rel="stylesheet" href="<?php echo base_url('') ?>css/administration/reset.css">
<link rel='stylesheet prefetch' href='http://fonts.googleapis.com/css?family=Roboto:400,100,300,500,700,900'>
<link rel='stylesheet prefetch' href='http://fonts.googleapis.com/css?family=Montserrat:400,700'>
<link rel='stylesheet prefetch' href='https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css'>
<link rel="stylesheet" href="<?php echo base_url('') ?>css/administration/style.css">
      
  </head>

  <body>    
<div class="container">
  <div class="info">
    <h1>SISTEMA DE GESTION DE BARBERIAS</h1>
  </div>
</div>
<div class="form">
 <div class="thumbnail"><img src="<?php echo base_url('') ?>images/fondo_login.svg"/></div>  
  
  <?=form_open('administration/Secure_bcrypt/register_bcrypt', $class='class="register-form"')?>
    <input type="text" name="username" placeholder="nombre"/>
    <input type="password" name="password" placeholder="password"/>
    <!--<input type="text" placeholder="email"/>-->
    <input type="hidden" name="token" value="<?=$token?>" />
    <button type="submit" name="submit">CREAR CUENTA</button>
    <p class="message">Estas Registrado? <a href="#">INGRESAR</a></p>
  <?=form_close()?>
  
  
  <?=form_open('administration/Secure_bcrypt/secure_login', 'class="login-form"');?>
    <input type="text" name="username" placeholder="nombre de usuario"/><p><?=form_error('username')?></p>
    <input type="password" name="password" placeholder="contraseña"/><p><?=form_error('password')?></p>
    <input type="hidden" name="token" value="<?=$token?>" />	
    <button type="submit" name="submit" >ENTRAR</button>
    <p class="message">No estas registrado? <a href="#">Crear Cuenta</a></p>
  <?=form_close()?>  
  </div>
    <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
    <script src="<?php echo base_url('') ?>js/administration/index.js"></script>
  </body>
</html>