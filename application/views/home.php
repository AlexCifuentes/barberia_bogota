<?php
//echo $fechaNow;
?>
<!--vista del calendario para reservar view/home-->
<!DOCTYPE html>
<html>
    <head>
    <title><?=$titulo?></title>
        <meta charset='utf-8' />
        <link rel='stylesheet' href='<?php echo base_url()?>fullcalendar-2.9.0/lib/cupertino/jquery-ui.min.css' />
        <link href='<?php echo base_url()?>fullcalendar-2.9.0/fullcalendar.css' rel='stylesheet' />
        <link href='<?php echo base_url()?>fullcalendar-2.9.0/fullcalendar.print.css' rel='stylesheet' media='print' />
        <link href="<?php echo base_url()?>bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo base_url();?>assets_fullcalendar/css/bootstrap-colorpicker.min.css" rel="stylesheet" />
        <link href="<?php echo base_url();?>assets_fullcalendar/css/bootstrap-timepicker.min.css" rel="stylesheet" />
        <link href="<?php echo base_url();?>assets_fullcalendar/css/timepicker.less" type="text/css" href="css/timepicker.less" />

        <style>

            body {
                margin: 40px 10px;
                padding: 0;
                font-family: "Lucida Grande", Helvetica, Arial, Verdana, sans-serif;
                font-size: 14px;
            }
            .fc th {
                padding: 10px 0px;
                vertical-align: middle;
                background:#F2F2F2;
            }
            .fc-day-grid-event>.fc-content {
                padding: 4px;
            }
            #calendar {
                max-width: 900px;
                margin: 0 auto;
            }
            .error {
                color: #ac2925;
                margin-bottom: 15px;
            }
            .event-tooltip {
                width:150px;
                background: rgba(0, 0, 0, 0.85);
                color:#FFF;
                padding:10px;
                position:absolute;
                z-index:10001;
                -webkit-border-radius: 4px;
                -moz-border-radius: 4px;
                border-radius: 4px;
                cursor: pointer;
                font-size: 11px;

            }
            .modal-header
            {
                background-color: #3A87AD;
                color: #fff;
            }
        </style>
    </head>
    <body background="<?php echo base_url()?>/images/back.jpg">    

        <div class="container">
            <div class="row">
                <div class="col-xs-20 col-md-8">
                    <a href="<?php echo site_url('')?>">
                        <button type="button" class="btn btn-success">
                        <span class="glyphicon glyphicon-home"></span>
                    </button>
                    </a>
                </div>
                <div class="col-xs-12 col-md-8">
                    &nbsp;
                </div>
            </div>
            
            
            <div class="row">
              <div class="col-xs-6 col-md-4">&nbsp;</div>
            </div>
           
        
            <div class="row clearfix">
                <div class="col-md-12 column">
                        <div id='calendar'></div>
                </div>
            </div>
        </div>
        <div class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title"></h4>
                    </div>
                    <div class="modal-body">
                        <div class="error"></div>
                        <form class="form-horizontal" id="crudform">
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="cedula" >Cedula</label>
                                <div class="col-md-4">
                                    <input id="cedula" name="cedula" type="text" class="form-control input-md" onkeypress="return valida(event)" />
                                </div>
                            </div>
                            <div class="form-group">                                
                                <label class="col-md-4 control-label" for="time">Horario L-D 9:00 a 19:00</label>
                                <div class="col-md-4 input-append bootstrap-timepicker">
                                    <input id="time" name="time" type="text" class="form-control input-md" />
                                    <span class="help-block">Formato 24 horas reservas de 9:00 a 19:00 </span>
                                </div>  
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="idbarbero">Barbero</label>
                                <div class="col-md-4">
                                    <select name="idbarbero" id="idbarbero" class="form-control">
                                        <?php
                                        foreach ($barberos as $br) {
                                            echo "<option value=".$br->id.">".$br->nombre." ".$br->apellido."</option>";
                                        }
                                        ?>
                                    </select>                                    
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="color">Color</label>
                                <div class="col-md-4">
                                    <input id="color" name="color" type="text" class="form-control input-md" readonly="readonly" />
                                    <span class="help-block">Click para pintar de un color</span>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    </div>
                </div>
            </div>
        </div>
        <script src='<?php echo base_url()?>assets_fullcalendar/js/jquery.min.js'></script>
        <script src='<?php echo base_url()?>fullcalendar-2.9.0/lib/moment.min.js'></script>
        <script src='<?php echo base_url()?>bootstrap/dist/js/bootstrap.min.js'></script>
        <script type="text/javascript" src="<?php echo base_url()?>assets_fullcalendar/js/bootstrap-datetimepicker.js" charset="UTF-8"></script>
        <script src='<?php echo base_url();?>assets_fullcalendar/js/bootstrap-colorpicker.min.js'></script>
        <script src='<?php echo base_url();?>assets_fullcalendar/js/bootstrap-timepicker.min.js'></script>        
        <script src='<?php echo base_url()?>fullcalendar-2.9.0/fullcalendar.min.js'></script>
        <script src='<?php echo base_url()?>fullcalendar-2.9.0/lang-all.js'></script>
        <script src='<?php echo base_url();?>jquery-validation-1.15.0/dist/jquery.validate.min.js'></script>
        <script src='<?php echo base_url();?>assets_fullcalendar/js/main.js'></script>        
        <!--<script src='<?php echo base_url();?>assets/js/validar.js'></script>-->
    <script type="text/javascript">
    //valido que campo cedula sea solo numeros
    
       function valida(e){

        tecla = (document.all) ? e.keyCode : e.which;
        //Tecla de retroceso para borrar, siempre la permite
        if (tecla==8){
            return true;
            }
        // Patron de entrada, en este caso solo acepta numeros
        patron =/[0-9]/;
        tecla_final = String.fromCharCode(tecla);
        return patron.test(tecla_final);
        }         
        </script>




   
    </body>
</html>



