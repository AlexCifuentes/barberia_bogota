<!DOCTYPE HTML>
<!--
    Visualize by TEMPLATED
    templated.co @templatedco
    Released for free under the Creative Commons Attribution 3.0 license (templated.co/license)
-->
<html>
    <head>
        <title>Barberia Bogota | Cortes-Barbas</title>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="stylesheet" href="assets/css/main.css" />
        <link type="text/css" rel="stylesheet" href="css/jquery.mmenu.css" />
        <link type="text/css" rel="stylesheet" href="css/jquery.mmenu.all.css" />
        <link rel="stylesheet" href="bootstrap/jasny-bootstrap/css/jasny-bootstrap.min.css">
         <!-- Custom styles for this template -->
    <link href="bootstrap/jasny-bootstrap/css/navmenu-push.css" rel="stylesheet"> 
    </head>
    <body>
        <div>
        </div>
        
        <!-- menu -->
        <div class="navmenu navmenu-default navmenu-fixed-left offcanvas">
      <a class="navmenu-brand" href="#">Barbeia Bogotá</a>
      <ul class="nav navmenu-nav">
        <li><a href="<?php echo site_url('Welcome/mision') ?>">Mision</a></li>
        <li><a href="<?php echo site_url('Welcome/Vision') ?>">Vision</a></li>
        <li><a href="<?php echo site_url('Welcome/Baberos') ?>">Baberos</a></li>
        <li><a href="<?php echo base_url();?>calendar">Resevas</a></li>
        <li><a href="<?php echo site_url('Usuario/index') ?>">Tus Cortes</a></li>
        <li><a href="<?php echo site_url('Welcome/desarrollo') ?>">Desarrollo</a></li>
        <li><a href="<?php echo site_url('administration/Secure_bcrypt') ?>">Administrar</a></li>
        <li><a href="<?php echo site_url('Welcome/test') ?>">test</a></li>
      </ul>
     </div>
     <!-- END MENU -->
    <!-- BOTTON MENU -->
    <div class="navbar navbar-default navbar-fixed-top">
      <button type="button" class="icon style2 fa-bars" data-toggle="offcanvas" data-target=".navmenu" data-canvas="body">       
      </button>
    </div> 
     <!-- END BOTTON MENU -->                 

        <!-- Wrapper -->
            <div id="wrapper">

                <!-- Header -->
                    <header id="header">
                        <span class="avatar"><img src="images/avatar.png" alt="" /></span>
                        <h1>Un lugar para<strong> HOMBRES </strong>con estilo</h1>
                        
                        <ul class="icons">
                            <li><a href="#" class="icon style2 fa-twitter"><span class="label">Twitter</span></a></li>
                            <li><a href="https://es-es.facebook.com/people/Barber%C3%ADa-Bta/100009208883799" class="icon style2 fa-facebook"><span class="label">Facebook</span></a></li>
                            <li><a href="https://www.instagram.com/barberia_bogota/" class="icon style2 fa-instagram"><span class="label">Instagram</span></a></li>
                            <li><a href="#" class="icon style2 fa-500px"><span class="label">500px</span></a></li>
                            <li><a href="#" class="icon style2 fa-envelope-o"><span class="label">Email</span></a></li>
                        </ul> 
                        
                    </header>

                <!-- Main -->
                    <section id="main">

                        <!-- Thumbnails -->
                            <section class="thumbnails">
                                <div id="cortes">
                                    <?php
                                    //pinto las imagenes
                                    foreach($imagenes as $img){
                                        $src = $img->ruta;
                                        $nombre= $img->nombre;                                        
                                        echo '
                                        <a href="'.$src.'">
                                        <img src="'.$src.'" alt="" />
                                        <h3>'.$nombre.'</h3>
                                        
                                        </a>
                                        ';
                                    }
                                    ?>
                                                                        
                                </div>
                                <div id="barbas">
                                    
                                </div>
                                
                                <div id="cejas">
                                    
                                </div>
                                
                                <div id="images"></div>
                                
                                
                            </section>

                    </section>

                <!-- Footer -->
                    <footer id="footer">
                        <!--<p>&copy; Untitled. All rights reserved. Design: <a href="http://templated.co">TEMPLATED</a>. Demo Images: <a href="http://unsplash.com">Unsplash</a>.</p>-->
                    </footer>

            </div>

        <!-- Scripts -->
            <script src="assets/js/jquery.min.js"></script>
            <script src="assets/js/jquery.poptrox.min.js"></script>
            <script src="assets/js/skel.min.js"></script>
            <script src="assets/js/main.js"></script>
            <script src="js/photoswipe/photoswipe.min.js"></script> 
            <script src="js/photoswipe/photoswipe-ui-default.min.js"></script>
        
        <!-- Scripts contol de las imagenes -->
            <script src="js/imagenes/imagenes.js"></script>
        
         <!-- Scripts menu -->
            <script type="text/javascript" src="bootstrap/jasny-bootstrap/js/jasny-bootstrap.min.js"></script>
            <script type="text/javascript">
            </script>
    </body>
</html>