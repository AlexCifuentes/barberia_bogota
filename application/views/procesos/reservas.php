<!DOCTYPE HTML>
<!--
	Identity by HTML5 UP
	html5up.net | @ajlkn
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
esta pagina tiene un mal diseño se devido a conflicto con stilos psteiormente corregir
-->
<html>
	<head>
		<title><?=$titulo?></title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<!--[if lte IE 8]><script src="assets/js/html5shiv.js"></script><![endif]-->
		<link rel="stylesheet" href="../../assets-2/css/main.css" />
		<!--[if lte IE 9]><link rel="stylesheet" href="assets/css/ie9.css" /><![endif]-->
		<!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie8.css" /><![endif]-->
		<noscript><link rel="stylesheet" href="../../assets-2/css/noscript.css" /></noscript>
	</head>
	<body class="is-loading">
        
        
		<!-- Wrapper -->
			<div id="wrapper">
				<!-- Main -->
					<section id="main">                        
						<header>
							<span class="avatar">
                            <ul class="icons">
								<li><a onclick="goBack()" class="fa-arrow-left">volver</a></li>
								<li><a href="<?php echo site_url('')?>" class="fa-home">Inicio</a></li>
							</ul>
                            </span>
							<h1>Realizar Reserva</h1>
							<p>Realice reservas de turno desde aqui</p>						                        
                       
                        </header>
                        

                        <!-- Form uplaad images -->
                         <form action="<?php echo site_url('Reservas/reservando'); ?>">
                             <div class="field">
                                 <span>Ingrese su cedula</span>
                             <input type="number" name="cedula" id="cedula">
                             </div>
                             <div class="field">
                             <span>Seleccione dia</span>
                             <input type="date" name="fecha" id="fecha">
                             </div>
                             <div class="field">
                             <span>Seleccione hora</span>
                             <input type="text" name="hora" id="hora">
                             </div>
                             <div class="field">
                                 <span>Seleccione barbero</span>
                             <input type="text" name="barbero" id="barbero">
                             </div>
                             <div class="field">
                                 <button>Resevar</button>
                             </div>
                        </form>
                        
						<footer>
							<!--<ul class="icons">
								<li><a href="#" class="fa-twitter">Twitter</a></li>
								<li><a href="#" class="fa-instagram">Instagram</a></li>
								<li><a href="#" class="fa-facebook">Facebook</a></li>
							</ul>-->
						</footer>
					</section>

				<!-- Footer -->
					<footer id="footer">
						<ul class="copyright">
							<li>&copy;</li><li>Design: <a href="http://html5up.net">HTML5 UP</a></li>
						</ul>
					</footer>

			</div>
        

		<!-- Scripts -->
        <script src="../../assets/js/jquery.min.js"></script>
			<!--[if lte IE 8]><script src="assets/js/respond.min.js"></script><![endif]-->
			<script>
				if ('addEventListener' in window) {
					window.addEventListener('load', function() { document.body.className = document.body.className.replace(/\bis-loading\b/, ''); });
					document.body.className += (navigator.userAgent.match(/(MSIE|rv:11\.0)/) ? ' is-ie' : '');
				}
			</script>
            <script>
                function goBack() {
                    window.history.back();
                }
            </script>
	</body>
</html>