<!DOCTYPE HTML>
<!--Vista encargada de subir las imagenes de cortes-->
<!--
	Identity by HTML5 UP
	html5up.net | @ajlkn
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
esta pagina tiene un mal diseño se devido a conflicto con stilos psteiormente corregir
-->
<html>
	<head>
		<title><?=$titulo?></title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<!--[if lte IE 8]><script src="assets/js/html5shiv.js"></script><![endif]-->
		<link rel="stylesheet" href="<?php echo base_url('') ?>/assets-2/css/main.css" />
		<!--[if lte IE 9]><link rel="stylesheet" href="assets/css/ie9.css" /><![endif]-->
		<!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie8.css" /><![endif]-->
		<noscript><link rel="stylesheet" href="<?php echo base_url('') ?>/assets-2/css/noscript.css" /></noscript>
        <link href="<?php echo base_url(); ?>dropzone/dist/min/dropzone.min.css" type="text/css" rel="stylesheet" />
	</head>
	<body class="is-loading">
        
        
		<!-- Wrapper -->
			<div id="wrapper">
				<!-- Main -->
					<section id="main">                        
						<header>
							<span class="avatar">
                            <!--<ul class="icons">
								<li><a onclick="goBack()" class="fa-arrow-left">volver</a></li>
								<li><a href="<?php echo site_url('')?>" class="fa-home">Inicio</a></li>
							</ul>-->
                            </span>
							<h1>Subir Mision de la empresa</h1>
							<p>Arrastre y suelte achivos, o solo haga click dentro del cuadro</p>						                        
                       
                        </header>
                        

                        <!-- Form uplaad images -->
                         <form action="<?php echo site_url('/dropzone/uploadMision'); ?>" class="dropzone"  >
                        </form>
                            
						<!--
						<hr />
						<h2>Extra Stuff!</h2>
						<form method="post" action="#">
							<div class="field">
								<input type="text" name="name" id="name" placeholder="Name" />
							</div>
							<div class="field">
								<input type="email" name="email" id="email" placeholder="Email" />
							</div>
							<div class="field">
								<div class="select-wrapper">
									<select name="department" id="department">
										<option value="">Department</option>
										<option value="sales">Sales</option>
										<option value="tech">Tech Support</option>
										<option value="null">/dev/null</option>
									</select>
								</div>
							</div>
							<div class="field">
								<textarea name="message" id="message" placeholder="Message" rows="4"></textarea>
							</div>
							<div class="field">
								<input type="checkbox" id="human" name="human" /><label for="human">I'm a human</label>
							</div>
							<div class="field">
								<label>But are you a robot?</label>
								<input type="radio" id="robot_yes" name="robot" /><label for="robot_yes">Yes</label>
								<input type="radio" id="robot_no" name="robot" /><label for="robot_no">No</label>
							</div>
							<ul class="actions">
								<li><a href="#" class="button">Get Started</a></li>
							</ul>
						</form>
						<hr />
						-->
                        
                       
                        
						<footer>
							<!--<ul class="icons">
								<li><a href="#" class="fa-twitter">Twitter</a></li>
								<li><a href="#" class="fa-instagram">Instagram</a></li>
								<li><a href="#" class="fa-facebook">Facebook</a></li>
							</ul>-->
						</footer>
					</section>

				<!-- Footer -->
					<footer id="footer">
						<ul class="copyright">
                            <li>&copy;</li><li>Desarrollo: <a href="http://html5up.net">LIKANSOFT</a></li>
							<li>&copy;</li><li>Design: <a href="http://html5up.net">HTML5 UP</a></li>
						</ul>
					</footer>

			</div>
        

		<!-- Scripts -->
        <script src="<?php echo base_url('') ?>/js/jquery.min.js"></script>
        <script src="<?php echo base_url(); ?>dropzone/dist/dropzone.js"></script>
        <!--<script src="<?php echo base_url(); ?>dropzone/dist/min/dropzone.min.js"></script>-->
			<!--[if lte IE 8]><script src="assets/js/respond.min.js"></script><![endif]-->
			<script>
				if ('addEventListener' in window) {
					window.addEventListener('load', function() { document.body.className = document.body.className.replace(/\bis-loading\b/, ''); });
					document.body.className += (navigator.userAgent.match(/(MSIE|rv:11\.0)/) ? ' is-ie' : '');
				}
			</script>
            <script>
                function goBack() {
                    window.history.back();
                }
            </script>
	</body>
</html>