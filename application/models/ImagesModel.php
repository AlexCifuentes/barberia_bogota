<?php
defined('BASEPATH') OR exit('No direct script access allowed');

 class ImagesModel extends CI_MODEL{

	function __construc(){
		parent::__construc();

	}
	
     function get_images(){
        //$this->db->order_by('nombre','asc');
        $imagenes= $this->db->get('imagenes');
         if($imagenes->num_rows()>0){
             return $imagenes->result();
             //return json_encode($imagenes->result());
         }
     }
     
     function get_cortes(){
        $this->db->where('tipo','corte');
        $imagenes= $this->db->get('imagenes');
         if($imagenes->num_rows()>0){
             return $imagenes->result();
             //return json_encode($imagenes->result());
         }
     }
     
     function get_barbas(){
        $this->db->where('tipo','barba');
        $imagenes= $this->db->get('imagenes');
         if($imagenes->num_rows()>0){
             return $imagenes->result();
             //return json_encode($imagenes->result());
         }
     }
     
     function get_cejas(){
        $this->db->where('tipo','cejas');
        $imagenes= $this->db->get('imagenes');
         if($imagenes->num_rows()>0){
             return $imagenes->result();
             //return json_encode($imagenes->result());
         }
     }
      
}