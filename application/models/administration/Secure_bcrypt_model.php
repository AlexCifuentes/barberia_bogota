<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 include("d_base/d_base.php"); 

class Secure_bcrypt_model extends CI_Model {
	
	public function __construct() {
		parent::__construct();
	}
	
	public function save_pass($username,$hash)
	{
		$data = array(
			'usuario'	=>		$username,
			'password'		=>		$hash,
			'nombre'	=>		$username,
		);
		return $this->db->insert('us_usuarios',$data);
	}
	
	public function login($username,$hash)
	{
		$this->db->where('usuario',$username);
		$query = $this->db->get('us_usuarios');
		if($query->num_rows() == 1)
		{
			$user = $query->row();

			$pass = $user->password;

			$id_us = $user->idusuario;

			if($this->bcrypt->check_password($hash, $pass))
			{
				return $query->row();
			}else{
			$this->session->set_flashdata('usuario_incorrecto','Los datos introducidos son incorrectos');
			redirect('secure_bcrypt/login','refresh');
			}
		}
	}

}
