<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Secure_bcrypt_model extends CI_Model {
	
	public function __construct() {
		parent::__construct();
	}
	
	public function save_pass($username,$hash)
	{
		$data = array(
			'us_usuario'	=>		$username,
			'us_pwd'		=>		$hash
		);
		return $this->db->insert('us_usuarios',$data);
	}
	
	public function login($username,$hash)
	{
		$this->db->where('us_usuario',$username);
		$query = $this->db->get('us_usuarios');
		if($query->num_rows() == 1)
		{
			$user = $query->row();

			$pass = $user->us_pwd;

			$id_us = $user->id_us;

			if($this->bcrypt->check_password($hash, $pass))
			{
				return $query->row();
			}else{
			$this->session->set_flashdata('usuario_incorrecto','Los datos introducidos son incorrectos');
			redirect('secure_bcrypt/login','refresh');
			}
		}
	}

}
