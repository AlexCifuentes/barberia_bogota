<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('America/Bogota');//envio zona mundial
setlocale(LC_ALL,"es_CO");// envi set local
class Calendar_model extends CI_Model {
	//private fechaNow=date(YY:HH:mm)
		function __construc(){
				parent::__construc();

			}

/*Read the data from DB */
	Public function getEvents()
	{
		
	$sql = "SELECT * FROM reserva WHERE reserva.date BETWEEN ? AND ? ORDER BY reserva.date ASC";
	return $this->db->query($sql, array($_GET['start'], $_GET['end']))->result();

	}
	/*metodo que permite validar si la fecha esta disponible al momento de reservar*/
	Public function disponibleFecha()
	{
	$FECHA= strtotime(date("Y-m-d"));
	$DATE=$_POST['date'];
	$DATE=substr($DATE, 0, -9);
	$DATE=strtotime($DATE);
	/*valido la fecha enviada por el usuarrio */
	if ($FECHA < $DATE || $FECHA == $DATE) {
		//valido que no exita reserva en la fecha indicada pr el usuario
		$sql = "SELECT reserva.date, reserva.idbarbero FROM reserva WHERE reserva.date = ? AND reserva.idbarbero= ?";
        $sql = $this->db->query($sql, array($_POST['date'],$_POST['idbarbero']))->row();
        if (isset($sql)) {
        	return "ocupado";
        }
        //else{
        	//return true;
		//}
        //envio un true para indicar continuacion con proceso
        //return true;
	}else{
		$fechaPaso = "fechaPaso";//"La fecha entrada ya ha pasado";
		return $fechaPaso;		
	 }
	}

	/*metodo que permite validar si no hay reservas antes de la indicada por el usuario*/
	Public function validaHoraAnterior()
	{
	$FECHA= strtotime(date("Y-m-d"));
	$DATE=$_POST['date'];
	$DATE=substr($DATE, 0, -9);
	$DATE=strtotime($DATE);
	$HORA= strtotime(date("H:i"));
	$HORAU=$_POST['date'];
	$HORAU=substr($HORAU, 11, -3);
	$HORAU=strtotime($HORAU);
	/*valido la fecha enviado por el usuarrio */
	if($FECHA==$DATE AND $HORAU < $HORA){
		$horaPaso = "horaPaso";//"La fecha entrada ya ha pasado";
		return $horaPaso;
	}
	
	}

	/*metodo que permite validar si el barbero no descansa ese dia */
	Public function descansaBarbero()
	{
	//capturo la fecha enviada por el usuario
	$DATE=$_POST['date'];
	//capturo fecha sin horas (tiempo)
	$DATE=substr($DATE, 0, -9);
	//convierto y obtengo el valor numerico de la fecha de usuario
	//para validar el dia de descanso con la base de datos
	$numDia = date('N',strtotime($DATE));
	//obtengo el id del barbero
	//IDBARBERO=$_POST['idbarbero'];
	//consulto la tabla de barberos para saber si el babero no esta de descanso
	$sql = "SELECT barberos.dia_descanso FROM barberos WHERE barberos.id = ? ";
	//retorno la consulta como un array afectando la columna especifica 
	$diaDescansa=$this->db->query($sql, array($_POST['idbarbero']))->row_array();
	//asign retono de la consulta
	$diaDescansa=$diaDescansa['dia_descanso'];
		/*valido el numero de dia retornado por la consulta y valido con el numero de dia 
		*de fecha de reserva enviado por el usuario
		*/
		if ($diaDescansa == $numDia) {
			return "descansando";
		} 	
	}

	/*Create new reserva */

	Public function addEvent()
	{

	$sql = "INSERT INTO reserva (cedula,reserva.date, idbarbero, color) VALUES (?,?,?,?)";
	$this->db->query($sql, array($_POST['cedula'], $_POST['date'], $_POST['idbarbero'], $_POST['color']));
		return ($this->db->affected_rows()!=1)?false:true;
	}

	//sobrecargando metodo
	Public function addEventApi($cedula,$date,$idBarbero,$color)
	{
	$sql = "INSERT INTO reserva (cedula,reserva.date, idbarbero, color) VALUES (?,?,?,?)";
	$this->db->query($sql, array($cedula, $date, $idBarbero, $color));
		return ($this->db->affected_rows()!=1)?false:true;
	}
	

	/*Update  event */

	Public function updateEvent()
	{

	//$sql = "UPDATE events SET cedula = ?, events.date = ?, idbarbero = ?, color = ? WHERE id = ?";
	$sql = "UPDATE reserva SET cedula = ?, idbarbero = ?, color = ? WHERE id = ?";
	$this->db->query($sql, array($_POST['cedula'], $_POST['idbarbero'], $_POST['color'], $_POST['id']));
		return ($this->db->affected_rows()!=1)?false:true;
	}


	/*Delete event */

	Public function deleteEvent()
	{

	$sql = "DELETE FROM reserva WHERE id = ?";
	$this->db->query($sql, array($_GET['id']));
		return ($this->db->affected_rows()!=1)?false:true;
	}

	/*Update  event */

	Public function dragUpdateEvent()
	{
			$date=date('Y-m-d h:i:s',strtotime($_POST['date']));

			$sql = "UPDATE reserva SET  events.date = ? WHERE id = ?";
			$this->db->query($sql, array($date, $_POST['id']));
		return ($this->db->affected_rows()!=1)?false:true;


	}

	Public function list_barberos()
	{
		//$dia=getdate();//funcion permite obtener el dia actual representad por numeros
		//$diaNow=$dia['wday'];
		$sql = "SELECT * FROM barberos ORDER BY barberos.nombre ASC";
		return $this->db->query($sql)->result();
	}






}