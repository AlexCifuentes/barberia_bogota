<?php
/* correct json android
{
    "contacto": [
        {
            "idContacto": "C-709c040d-5cbb-4def-a248-444d1eba71f1",
            "primerNombre": "James",
            "primerApellido": "Revelo",
            "telefono": "1811918",
            "correo": "james@gmail.com",
            "idUsuario": "2",
            "version": "2015-12-23 12:24:28"
        }
    ],
    "estado": 100,
    "mensaje": "Sincronización completa"
}*/
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';
date_default_timezone_set('America/Bogota');//envio zona mundial
setlocale(LC_ALL,"es_CO");// envi set local

class Services_rest extends REST_Controller {
 
       function __construct(){
        parent:: __construct();
        //$this->load->helper(array('url','form','array','html'));
        $this->load->model(array('ImagesModel','TusCortesModel','Calendar_model'));
        $this->load->database('default');
        $this->load->library(array('REST_Controller','session','Format'));
        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        //$this->methods['user_get']['limit'] = 500; // 500 requests per hour per user/key
        //$this->methods['user_post']['limit'] = 100; // 100 requests per hour per user/key
        //$this->methods['user_delete']['limit'] = 50; // 50 requests per hour per user/key
    } 

    
    public function imagenes_get()
    {
     //consulto tabla imagenes
    $imagenes=$this->ImagesModel->get_images();
      
         if (!is_null($imagenes))
            {
                // Set the response and exit
                $this->response($imagenes, REST_Controller::HTTP_OK); // $this->response(array('response'=>$imagenes),200);OK (200) being the HTTP response code
            }
            else
            {
                // Set the response and exit
                $this->response([
                    'status' => FALSE,
                    'message' => 'No imagenes were found'
                ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
            }
    //$this->response($imagenes, 200);  
       
                
    }

    public function totalcortes_get($cedula)
    {
    //$cedula= (int)$cedula; 
    if (!$cedula) {
        $this->response(null, REST_Controller::HTTP_BAD_REQUEST);
    }
    //consulto tabla totalcortes
    $totalCortes=$this->TusCortesModel->post_totalCortes($cedula);
      
         if (!is_null($totalCortes))
            {
                // Set the response and exit
                //$this->response($totalCortes, REST_Controller::HTTP_OK);
                $this->response([
                    'estado' => "1",
                    'mensaje' => "Tienes un total de ".$totalCortes." cortes"
                ], REST_Controller::HTTP_OK);

                //$this->response(array($totalCortes),200);//OK (200) being the HTTP response code
            }
            else
            {
                // Set the response and exit
                $this->response([
                    'estado' => "2",
                    'mensaje' => 'No hay cortes Para este documento'
                ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
            }
    }
    
    public function nuevareserva_post()
    {
    //variables controlo hora actual del sistema 
    $FECHAACTUAL= strtotime(date("Y-m-d H:i"));
    //$HORAACTUAL= strtotime(date("H:i"));
    //$DATE=substr($DATE, 0, -9);
    //$DATE=strtotime($DATE);
    //recivo parametros
    $cedula=$this->input->post('cedula');
    $idBarbero=$this->post('idbarbero');
    $color='#ff1a00';
    $fecha=$this->post('fecha');
    $hora=$this->post('hora');
    $date = $fecha." ".$hora; 
    //convierto fecha y hora en valor legible para comparar    
    $fecha = strtotime($fecha." ".$hora);
    //consulto valor fecha y hora enviada no sean anterior a la del sitema limite establecido
    //valido si la fecha u hora no hayan pasado
    if($FECHAACTUAL > $fecha ){
        echo "La fecha u Hora ingresada ya ha pasado";
    }
    echo "string".$hora;
    if ($hora < "09:00" || $hora > "19:30") {
         echo "Lo sentimos a esa Hora esta cerrado intente nuevamente";
    }
    //valida si el barbero descanza
    //si no envio datos al modelo reserva 

    $nuevaReseva=$this->Calendar_model->addEventApi($cedula,$date,$idBarbero,$color);

    if (!is_null($nuevaReseva) || $nuevaReseva > 0)
            {
                // Set the response and exit
                $this->response($nuevaReseva, REST_Controller::HTTP_OK); 
            }
            else
            {
                // Set the response and exit
                $this->response([
                    'status' => FALSE,
                    'message' => 'No Se pudo Realizar la reserva por favor intente nuevamente'
                ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
            }
    
    }

   
    //metdo sacado de un tutria por corregir no funcional
   public function students_get()
{
    //$result = new Response_students($student, 200);    
 
    /*$id = $this->get('id');
    $student = $this->Consultas->datos_usuarios();
       
 
    if (!empty($equipment))
    {
        $result->responseCode    = 0;
        $result->responseMessage = 'Equipo encontrado';
        $result->student_data    = $student;
    }
    else
    {
        $result->responseCode    = 1;
        $result->responseMessage = 'El estudiante no existe';
        $result-> student_data   = null;
    }
        
    //$this->response($result, 200);
       $this->response($student, 200);*/

        $data = array('returned: '. $this->get('id'));
        $this->response($data);
}
    
   

    
}
