<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('America/Bogota');//envio zona mundial
setlocale(LC_ALL,"es_CO");// envi set local

class Calendar extends CI_Controller {


		function __construct()
    {
        // Call the Model constructor
        parent::__construct();
        $this->load->helper(array('url','form','array','html'));
        $this->load->model('Calendar_model');
        $this->load->database('default');
    }


	/*Home page Calendar view  */
	Public function index()
	{
		$data['titulo']= "RESERVAS";
		$data['barberos']=$this->Calendar_model->list_barberos();
		$data['fechaNow']= date("Y-m-d");
		$this->load->view('home',$data);		
	}

	/*Get all Events */

	Public function getEvents()
	{
		$result=$this->Calendar_model->getEvents();
		//retorno un objeto json leer documentacin para no olvidar http://php.net/manual/es/function.json-encode.php 
		foreach ($result as $key) {
			/*
			* como la consulta retorna un stdClasObject
			* creo un array asociativo que captura la consulta
			* y lo envio como un stdClassObject
			*/
			$resultado[] =				
					(object) array(
					"id" => $key->id,
					"title" => $key->cedula,
					"date" => $key->date,
					"cedula" => $key->cedula,
					"idbarbero" => $key->idbarbero,
					"color" => $key->color,
				);

		}
		//print_r($resultado);
		echo json_encode($resultado);
	}
	/*funcion que permite validar condicines para la reseva */
	Public function validaDatos()
	{
		//cnsulto fechas
		$fecha=$this->Calendar_model->disponibleFecha();
		//cnsulto descanso del barbero
		$descBarber=$this->Calendar_model->descansaBarbero();

		$horaPaso=$this->Calendar_model->validaHoraAnterior();

		//valida fecha no haya pasado
		if($fecha==="fechaPaso"){
			echo 'fechaPaso';
		}
		//valida si esta disponible el cupo
		elseif ($fecha==="ocupado") {
			echo 'ocupado';
		}
		//valido si el tiempo ya tiene turno o ya paso
		elseif ($descBarber==="descansando") {
			echo 'descansando';
		}
		// valido que la hora enviada por el usuario ya paso y es el mismo dia 
		//no permita reservar
		elseif ($horaPaso==="horaPaso") {
			echo 'horaPaso';
		}
		else{
			$this->addEvent();
			//echo 'error';
		}
		
	}

	/*Add new event */
	Public function addEvent()
	{
		$result=$this->Calendar_model->addEvent();
		echo $result;
		//echo "llego";
	}
	/*Update Event */
	Public function updateEvent()
	{
		$result=$this->Calendar_model->updateEvent();
		echo $result;
	}
	/*Delete Event*/
	Public function deleteEvent()
	{
		$result=$this->Calendar_model->deleteEvent();
		echo $result;
	}
	Public function dragUpdateEvent()
	{	

		$result=$this->Calendar_model->dragUpdateEvent();
		echo $result;
	}

	Public function test(){
	$FECHA= strtotime(date("Y-m-d"));
	$DATE=$_POST['date'];
	$DATE=substr($DATE, 0, -9);
	$DATE=strtotime($DATE);
	$HORA= strtotime(date("H:i"));
	$HORAU=$_POST['date'];
	$HORAU=substr($HORAU, 11, -3);
	$HORAU=strtotime($HORAU);
	print($HORA);
	echo "<br>";
	echo $HORAU;
	/*valido la fecha enviado por el usuario */
	if($FECHA==$DATE AND $HORAU < $HORA){
		echo "pasa fecha";
		//if ($HORAU < $HORA) {
		//$horaPaso = "horaPaso";//"La fecha entrada ya ha pasado";
		//echo "pasa hora";
		//}
	}
	
}



}
