<?php
defined('BASEPATH') OR exit('No direct script access allowed');

    
   class Dropzone extends CI_Controller {
  
    public function __construct() {
       parent::__construct();
       $this->load->helper(array('url','html','form'));
       $this->load->library(array('session','image_lib'));
       $this->load->database(); // load database
    }
 
    public function index() {
        $data['titulo']="Cargar Imagnes";
        $this->load->view('procesos/carga_imagenes',$data);
    }
    
    public function uploadMision() {
        if (!empty($_FILES)) {
        $tempFile = $_FILES['file']['tmp_name'];
        $fileName = $_FILES['file']['name'];
        $tipo_mime = $_FILES['file']['type'];
            //echo $tipo_mime;
        //si no existe la carpeta files la creamos
        if(!file_exists("./images/aplicacion/mision/")){
            mkdir("./images/aplicacion/mision/", 0777);
        }
        $ruta='images/aplicacion/mision/';
        $targetPath = getcwd() . '/'.$ruta;
        $targetFile = $targetPath . "mision.jpg" ;
        //muevo la imagen al path
        move_uploaded_file($tempFile, $targetFile);
        // Abro el archivo de imagen para cargar sus contenidos
        $archivo = 'images/aplicacion/mision/mision.jpg';
        $fp = fopen ($archivo, 'r');
        if ($fp){
            $datos = fread ($fp, filesize ($archivo)); // cargo la imagen
            fclose($fp);
            // averiguo su tipo mime
            //$tipo_mime = 'image/*';
            //$isize = imagesize($archivo);
            //if ($isize)
            //$tipo_mime = $isize['mime'];
            // La guardamos en la BD
            $datos = base64_encode ($datos);
            $fecha=date('Y-m-d');
            
            $datosActualizar = array(
                'nombre' => "mision",
                'imagen' => $datos,
                'tipo' => $tipo_mime,
                'ruta' => $ruta."mision.jpg",
                'fecha_subida' => $fecha
            );
            $this->db->set($datosActualizar);
            $this->db->where('nombre', "mision");
            $this->db->update('info_empresa');
            //$this->db->insert('info_empresa',array('nombre' => "mision",'imagen' => $datos,'tipo' => $tipo_mime,'ruta' => $ruta."mision.jpg",'fecha_subida' => $fecha));
             //llamo funcion para e dimensinar la imagen
            echo "string".$tempFile;
             $this->resize_image($targetFile, 50, 50, $tempFile);
            }//fin if fp
        }
    }
        
    public function uploadVision() {
        if (!empty($_FILES)) {
        $tempFile = $_FILES['file']['tmp_name'];
        $fileName = $_FILES['file']['name'];
        //si no existe la carpeta files la creamos
        if(!is_dir("images/aplicacion/vision/")){
            mkdir("images/aplicacion/vision/", 0777);
        }
        $ruta='images/aplicacion/vision/';
        $targetPath = getcwd() . '/'.$ruta;
        $targetFile = $targetPath . $fileName ;
        move_uploaded_file($tempFile, $targetFile);
        // if you want to save in db,where here
        // with out model just for example
        //$this->load->database(); // load database
        $fecha=date('Y-m-d');
        $this->db->insert('imagenes',array('nombre' => $fileName,'ruta' => $ruta.$fileName,'json_ruta' => $targetFile,'fecha_subida' => $fecha));
         $this->resize_image($ruta.$fileName, 50,50);
        }
    }

    /**
    *FUNCION CARGA LOS CORTES DE LA APLICACION
    */
    public function uploadCortes() {
        if (!empty($_FILES)) {
        $tempFile = $_FILES['file']['tmp_name'];
        $fileName = $_FILES['file']['name'];
        //si no existe la carpeta files la creamos
        if(!is_dir("images/aplicacion/cortes/")){
            mkdir("images/aplicacion/cortes/", 0777);
        }
        $ruta='images/aplicacion/cortes/';
        $targetPath = getcwd() . '/'.$ruta;
        $targetFile = $targetPath . $fileName ;
        move_uploaded_file($tempFile, $targetFile);
        // if you want to save in db,where here
        // with out model just for example
        //$this->load->database(); // load database
        $fecha=date('Y-m-d');
        $tipo='corte';
        $this->db->insert('imagenes',array('nombre' => $fileName,'ruta' => $ruta.$fileName,'json_ruta' => $targetFile,'fecha_subida' => $fecha,'tipo' => $tipo));
        //llamo funcion para e dimensinar la imagen
        $this->resize_image('./'.$ruta.$fileName, 50,50);
        }
    }

    /**
    *FUNCION CARGA LOS BARBAS DE LA APLICACION
    */
    public function uploadBarbas() {
        if (!empty($_FILES)) {
        $tempFile = $_FILES['file']['tmp_name'];
        $fileName = $_FILES['file']['name'];
        //si no existe la carpeta files la creamos
        if(!is_dir("images/aplicacion/barbas/")){
            mkdir("images/aplicacion/barbas/", 0777);
        }
        $ruta='images/aplicacion/barbas/';
        $targetPath = getcwd() . '/'.$ruta;
        $targetFile = $targetPath . $fileName ;
        move_uploaded_file($tempFile, $targetFile);
        // if you want to save in db,where here
        // with out model just for example
        //$this->load->database(); // load database
        $fecha=date('Y-m-d');
        $tipo='barba';
        $this->db->insert('imagenes',array('nombre' => $fileName,'ruta' => $ruta.$fileName,'json_ruta' => $targetFile,'fecha_subida' => $fecha,'tipo' => $tipo));
        //llamo funcion para e dimensinar la imagen
        $this->resize_image('./'.$ruta.$fileName, 50,50);
        }
    }

    /**
    *FUNCION CARGA LOS CEJAS DE LA APLICACION
    */
    public function uploadCejas() {
        if (!empty($_FILES)) {
        $tempFile = $_FILES['file']['tmp_name'];
        $fileName = $_FILES['file']['name'];
        //si no existe la carpeta files la creamos
        if(!is_dir("images/aplicacion/cejas/")){
            mkdir("images/aplicacion/cejas/", 0777);
        }
        $ruta='images/aplicacion/cejas/';
        $targetPath = getcwd() . '/'.$ruta;
        $targetFile = $targetPath . $fileName ;
        move_uploaded_file($tempFile, $targetFile);
        // if you want to save in db,where here
        // with out model just for example
        //$this->load->database(); // load database
        $fecha=date('Y-m-d');
        $tipo='ceja';
        $this->db->insert('imagenes',array('nombre' => $fileName,'ruta' => $ruta.$fileName,'json_ruta' => $targetFile,'fecha_subida' => $fecha,'tipo' => $tipo));
        //llamo funcion para e dimensinar la imagen
        $this->resize_image('./'.$ruta.$fileName, 50,50);
        }
    }

    function resize_image($file, $w, $h, $tempFile, $crop=FALSE) {
        var_dump($tempFile);
        //echo '*'.$tempFile;
        //require "/path/to/file"
        list($width, $height) = getimagesize($file);
        $r = $width / $height;
        if ($crop) {
            if ($width > $height) {
                $width = ceil($width-($width*abs($r-$w/$h)));
            } else {
                $height = ceil($height-($height*abs($r-$w/$h)));
            }
            $newwidth = $w;
            $newheight = $h;
        } else {
            if ($w/$h > $r) {
                $newwidth = $h*$r;
                $newheight = $h;
            } else {
                $newheight = $w/$r;
                $newwidth = $w;
            }
        }

        $src = imagecreatefromjpeg($file);
        $dst = imagecreatetruecolor($newwidth, $newheight);
        imagecopyresampled($dst, $src, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
        // Imprimir
        //imagejpeg($src, null, 100);
        move_uploaded_file($tempFile, $src);

        return $dst;
    }
}