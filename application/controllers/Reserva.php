<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';


class Reserva extends CI_Controller {


    function __construct(){
        parent:: __construct();
        $this->load->helper(array('url','form','array','html'));
        $this->load->model(array('ImagesController'));
        $this->load->database('default');
        $this->load->library(array('REST_Controller','session','Format'));
    }
    public function index()
	{
        $k=$this->session;
        $data['titulo']="Modulo Reservas";
		$this->load->view('procesos/reservas', $data);
	}
    
}
