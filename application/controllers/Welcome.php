<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';


class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
    function __construct(){
        parent:: __construct();
        $this->load->helper(array('url','form','array','html'));
        $this->load->model(array('ImagesModel'));
        $this->load->database('default');
        $this->load->library(array('REST_Controller','session','Format'));
    }
    public function index()
	{
        $k=$this->session;
        //print_r($k);
        //consulto tabla imagenes
        //$imagenes['imagenes']=$this->ImagesModel->get_images();
        //retorno un json
        //echo json_decode($imagenes);        
		$this->load->view('index');
	}

    
    public function get_cortes()
    {
        //$data['titulo']="Cargar Imagenes";
        
        $this->output->set_content_type('application/json');
        $imagenes=$this->ImagesModel->get_cortes();
        //print_r($imagenes);
        
        echo json_encode( $imagenes,true);
    }
    
    public function get_barbas()
    {
        
        $this->output->set_content_type('application/json');
        $imagenes=$this->ImagesModel->get_barbas();
                
        echo json_encode( $imagenes,true);
    }
    
    public function get_cejas()
    {
        
        $this->output->set_content_type('application/json');
        $imagenes=$this->ImagesModel->get_cejas();
        
        echo json_encode( $imagenes,true);
    }
    
    public function test()
    {
        $data['titulo']="Test";
        $this->load->view('archivo',$data);
    }
    
}
