<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Menu_master extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
		$this->load->model(array('administration/Consultas'));
		//$this->load->model('consultas');
		$this->load->library(array('session'));
		$this->load->helper(array('url','form','array'));
		$this->load->database('default');
    }
	
	//cargamos el formulario de registro
	public function index()
	{	
		$data['titulo'] = 'Administracion';
		$data['token'] = $this->token();
		$this->load->view('administration/menuMaster',$data);	
	}
	
	//creamos la clave aleatoria para agregar seguridad a nuestro formulario
	public function token()
	{
		$token = md5(uniqid(rand(),true));
		$this->session->set_userdata('token',$token);
		return $token;
	}
	
    //creamos metodo llama vista nosotros
	public function nosotros()
	{
		$data['titulo'] = 'Administracion';
		$data['token'] = $this->token();
		$this->load->view('administration/nosotros',$data);
	}
    
	//creamos metodo llama vista uiphp
	public function uiview()
	{
		$data['titulo'] = 'Administracion';
		$data['token'] = $this->token();
		$this->load->view('administration/ui',$data);
	}
    
    //creamos metodo llama vista cortesphp
	public function subirCortes()
	{
		$data['titulo'] = 'Administrar Cortes';
		$data['token'] = $this->token();
		$this->load->view('administration/cortes',$data);
	}

	//creamos metodo llama vista blanphp
	public function blankview()
	{
		$data['titulo'] = 'Administracion';
		$data['token'] = $this->token();
		$this->load->view('administration/blank',$data);
	}

	//cerramos sesión
	public function logout_bcrypt()
	{
		$this->session->sess_destroy();
		redirect('secure_bcrypt');
	}
    //metodo carga vista para cargar la mision al aplicativo
     public function carga_mision()
    {
        $data['titulo']="Cargar Mision";
        $this->load->view('procesos/carga_mision',$data);
    }
    //metodo carga vista para cargar cortes al aplicativo
     public function carga_cortes()
    {
        $data['titulo']="Cargar Cortes";
        $this->load->view('procesos/carga_cortes',$data);
    }
}
/*
 * end controllers/secure_bcrypt.php
 */
