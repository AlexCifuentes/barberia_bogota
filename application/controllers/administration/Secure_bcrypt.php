<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Secure_bcrypt extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
		$this->load->library('bcrypt');//cargamos la librería
		$this->load->model(array('administration/Secure_bcrypt_model','administration/Consultas'));
		//$this->load->model('consultas');
		$this->load->library(array('form_validation','session'));
		$this->load->helper(array('url','form','array'));
		$this->load->database('default');
    }
	
	//cargamos el formulario de registro
	public function index()
	{	
		$data['titulo'] = 'Login seguro';
		$data['token'] = $this->token();
		$this->load->view('administration/login_view',$data);	
	}
	
	//creamos la clave aleatoria para agregar seguridad a nuestro formulario
	public function token()
	{
		$token = md5(uniqid(rand(),true));
		$this->session->set_userdata('token',$token);
		return $token;
	}
	
	//procesamos los datos del formulario de secure_view.php para un nuevo usuario
	public function register_bcrypt()
	{
		//si existe la clave token oculta en el formulario y es igual
		// que la generada con el método token dejamos pasar
		if($this->input->post('token') && $this->input->post('token') == $this->session->userdata('token'))
		{
			$this->form_validation->set_rules('username', 'nombre de usuario', 'required|trim|min_length[2]|max_length[150]');
	       	$this->form_validation->set_rules('password', 'password', 'required|trim|min_length[4]|max_length[150]');
	 
	        //lanzamos mensajes de error si es que los hay
	        $this->form_validation->set_message('required', 'El %s es requerido');
	        $this->form_validation->set_message('min_length', 'El %s debe tener al menos %s carácteres');
	        $this->form_validation->set_message('max_length', 'El %s debe tener al menos %s carácteres');
			
			//si el proceso falla mostramos errores
			if($this->form_validation->run() == FALSE)
			{				
				redirect($this->index());
			//en otro caso procesamos los datos
			}else{
				$username = $this->input->post('username');
				$password = $this->input->post('password');
				
				$hash = $this->bcrypt->hash_password($password);
				
				//comprobamos si el password se ha encriptado
				if ($this->bcrypt->check_password($password, $hash))
				{
				    $insert_password = $this->Secure_bcrypt_model->save_pass($username,$hash);
					if($insert_password)
					{
						//redirect('secure_bcrypt/login');
						//echo 'satisfactrio';
						$this->login();
						
					}
				}
			}
		}
	}
	
	//cargamos el formulario de login
	public function login()
	{
		$data['titulo'] = 'Login seguro ';
		$data['token'] = $this->token();
		$this->load->view('administration/login_view',$data);	
	}

	//cargamos la vista confirmando los datos del usuario una vez logueado
	public function login_ok()
	{
		//$data['titulo'] = 'Login correcto con Bcrypt en codeigniter';				
		$this->load->view('administration/menuMaster');
			
	}
	
	public function view_menuprincipal()
	{
        $data =array(
            'titulo' => 'aplicativo',
		    'sql01' => $this->consultas->sql01(),
		    'sql00' => $this->consultas->sql00(),
            'sql02' =>$this->consultas->sql02());		
        $this->load->view('principal/menuprincipal',$data);
	}

	
	//procesamos los datos del formulario de login_view.php
	public function secure_login()
	{
		//si existe la clave token oculta en el formulario y es igual
		// que la generada con el método token dejamos pasar
		if($this->input->post('token') && $this->input->post('token') == $this->session->userdata('token'))
		{
			$this->form_validation->set_rules('username', 'nombre de usuario', 'required|trim|min_length[2]|max_length[150]');
	       	$this->form_validation->set_rules('password', 'password', 'required|trim|min_length[3]|max_length[150]');
	 
	        //lanzamos mensajes de error si es que los hay
	        $this->form_validation->set_message('required', 'El %s es requerido');
	        $this->form_validation->set_message('min_length', 'El %s debe tener al menos %s carácteres');
	        $this->form_validation->set_message('max_length', 'El %s debe tener al menos %s carácteres');
			
			//si el proceso falla mostramos errores
			if($this->form_validation->run() == FALSE)
			{
				//echo "string2";
				$this->login();
				
			//en otro caso procesamos los datos
			}else{
				$username = $this->input->post('username');
				$password = $this->input->post('password');
				$user 	  = $this->input->post('username');
						
				$login = $this->Secure_bcrypt_model->login($username,$password);
				$datos_usuario = $this->Consultas->datos_usuario($user);
				
				//si existe el usuario hacemos login y creamos sesiones		
				if($login)
				{
					$data = array(
		                'is_logued_in' 	=> 		TRUE,
		                'username' 		=> 		$login->usuario,
		                'password'		=>		$login->password		                
	            	);		
					$this->session->set_userdata($data);				
					$this->login_ok();
				}

				if($datos_usuario)
				{
					$datos = array(
		                'is_logued_in' 	=> 		TRUE,
		                'us_id' 		=> 		$datos_usuario->idusuario,
		                //'perfil'		=>		$datos_usuario->us_perfil,
		                //'rol'			=>		$datos_usuario->ru_id,
                        'us_nombre'     =>		$datos_usuario->nombre,
                        
	            	);		
					$this->session->set_userdata($datos);
					//$this->consultas->sql01($datos);				
					//$this->login_ok();					
				}
				//si no coinciden los datos recagamos la vista login
				else{
					echo '<script type="text/javascript">alert("Usuario o contraseña incorectos intente de nuevo!");</script>';
					$this->login();
				}

			}
		}else{
			echo 'Error con el token comuniquese con el administrador';
		}
	}

	//cerramos sesión
	public function logout_bcrypt()
	{
		$this->session->sess_destroy();
		redirect('secure_bcrypt');
	}
}
/*
 * end controllers/secure_bcrypt.php
 */
